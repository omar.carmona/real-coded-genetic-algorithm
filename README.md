# Real-Coded Genetic Algorithm

The purpose of this code is to show how real-coded genetic algorithms can be implemented in R from scratch. The code provides two selection methods, four crossover methods, and two mutations.

If you use it, please refer to the paper:

Cortes, O. A. C and Silva, J., "Unconstrained numerical optimization using real-coded genetic algorithms: a study case using benchmark functions in R from Scratch", RBCA, vol. 11, nº 3, p. 1-11, set. 2019.

Or use the following code in your bibtex:

@article{Cortes2019, <br />
  title={Unconstrained numerical optimization using real-coded genetic algorithms: a study case using benchmark functions in R from Scratch}, <br />
  volume={11}, <br />
  url={http://seer.upf.br/index.php/rbca/article/view/9047}, <br />
  DOI={10.5335/rbca.v11i3.9047}, <br />
  number={3}, <br />
  journal={Revista Brasileira de Computação Aplicada}, <br />
  author={Cortes, O. A. C. and Silva, J.}, <br />
  year={2019}, <br />
  month={set.}, <br />
  pages={1--11}<br />
}